package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MarsRoverTurnRight {
    @Test
    void should_only_change_Location_north_direction_when_execute_command_given_location_and_command_turn_right() {
        Location location = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(location);
        TurnRight turnRight = new TurnRight();

        Invoker invoker = new Invoker();
        invoker.setCommands(turnRight);
        invoker.execute(marsRover);

        Assertions.assertSame(0, marsRover.getLocation().getX());
        Assertions.assertSame(0, marsRover.getLocation().getY());
        Assertions.assertSame(Direction.North, marsRover.getLocation().getDirection());

    }

    @Test
    void should_only_change_Location_east_direction_when_execute_command_given_location_and_command_turn_right() {
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(location);
        TurnRight turnRight = new TurnRight();

        Invoker invoker = new Invoker();
        invoker.setCommands(turnRight);
        invoker.execute(marsRover);

        Assertions.assertSame(0, marsRover.getLocation().getX());
        Assertions.assertSame(0, marsRover.getLocation().getY());
        Assertions.assertSame(Direction.East, marsRover.getLocation().getDirection());

    }

    @Test
    void should_only_change_Location_south_direction_when_execute_command_given_location_and_command_turn_right() {
        Location location = new Location(0,0,Direction.East);
        MarsRover marsRover = new MarsRover(location);
        TurnRight turnRight = new TurnRight();

        Invoker invoker = new Invoker();
        invoker.setCommands(turnRight);
        invoker.execute(marsRover);

        Assertions.assertSame(0, marsRover.getLocation().getX());
        Assertions.assertSame(0, marsRover.getLocation().getY());
        Assertions.assertSame(Direction.South, marsRover.getLocation().getDirection());

    }

    @Test
    void should_only_change_Location_west_direction_when_execute_command_given_location_and_command_turn_right() {
        Location location = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(location);
        TurnRight turnRight = new TurnRight();

        Invoker invoker = new Invoker();
        invoker.setCommands(turnRight);
        invoker.execute(marsRover);

        Assertions.assertSame(0, marsRover.getLocation().getX());
        Assertions.assertSame(0, marsRover.getLocation().getY());
        Assertions.assertSame(Direction.West, marsRover.getLocation().getDirection());

    }
}
