package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class MarsRoverMoveTest {
    @Test
    void should_only_plus_Location_y_1_when_execute_command_given_location_and_command() {
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(location);
        Move move = new Move();


        Invoker invoker = new Invoker();
        invoker.setCommands(move);
        invoker.execute(marsRover);

        Assertions.assertSame(0, marsRover.getLocation().getX());
        Assertions.assertSame(1, marsRover.getLocation().getY());
        Assertions.assertSame(Direction.North, marsRover.getLocation().getDirection());

    }

    @Test
    void should_only_plus_Location_x_1_when_execute_command_given_location_and_command() {
        Location location = new Location(0,0,Direction.East);
        MarsRover marsRover = new MarsRover(location);
        Move move = new Move();

        Invoker invoker = new Invoker();
        invoker.setCommands(move);
        invoker.execute(marsRover);

        Assertions.assertSame(1, marsRover.getLocation().getX());
        Assertions.assertSame(0, marsRover.getLocation().getY());
        Assertions.assertSame(Direction.East, marsRover.getLocation().getDirection());

    }

    @Test
    void should_only_sub_Location_y_1_when_execute_command_given_location_and_command() {
        Location location = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(location);
        Move move = new Move();

        Invoker invoker = new Invoker();
        invoker.setCommands(move);
        invoker.execute(marsRover);

        Assertions.assertSame(0, marsRover.getLocation().getX());
        Assertions.assertSame(-1, marsRover.getLocation().getY());
        Assertions.assertSame(Direction.South, marsRover.getLocation().getDirection());

    }
    @Test
    void should_only_sub_Location_x_1_when_execute_command_given_location_and_command() {
        Location location = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(location);
        Move move = new Move();

        Invoker invoker = new Invoker();
        invoker.setCommands(move);
        invoker.execute(marsRover);

        Assertions.assertSame(-1, marsRover.getLocation().getX());
        Assertions.assertSame(0, marsRover.getLocation().getY());
        Assertions.assertSame(Direction.West, marsRover.getLocation().getDirection());

    }





    @Test
    void should_change_Location_x_y_direction_when_execute_command_given_location_and_commands() {
        Location location = new Location(0,0,Direction.North);

        MarsRover marsRover = new MarsRover(location);


        List<Command> commands = new ArrayList<>();
        commands.add(new TurnRight());
        commands.add(new Move());
        commands.add(new TurnLeft());
        commands.add(new Move());


        Invoker invoker = new Invoker();
        invoker.setCommands(commands);
        invoker.execute(marsRover);

        Assertions.assertSame(1, marsRover.getLocation().getX());
        Assertions.assertSame(1, marsRover.getLocation().getY());
        Assertions.assertSame(Direction.North, marsRover.getLocation().getDirection());

    }


}