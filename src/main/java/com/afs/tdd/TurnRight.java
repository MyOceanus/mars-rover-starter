package com.afs.tdd;

public class TurnRight implements Command {


    private final Direction[] directionArray = new Direction[4];

    private int directionIndex;

    private void initDirectionArray() {
        directionArray[0] = Direction.North;
        directionArray[1] = Direction.East;
        directionArray[2] = Direction.South;
        directionArray[3] = Direction.West;
    }

    public TurnRight(){
        initDirectionArray();
    }

    @Override
    public void execute(Location location) {
        if (location != null){
            for (int i = 0; i < directionArray.length; i++) {
                if (location.getDirection() == directionArray[i]) {
                    directionIndex = i;
                    break;
                }
            }
            directionIndex = (directionIndex + 1) % 4;
            location.setDirection(directionArray[directionIndex]);
        }
    }
}
