package com.afs.tdd;

import java.util.HashMap;
import java.util.Map;

public class Move implements Command {

    private Map<Direction,Integer[]> directionMoveMap = new HashMap<>();

    private void initDirectionMoveMap(){
        directionMoveMap.put(Direction.North, new Integer[]{0, 1});
        directionMoveMap.put(Direction.South, new Integer[]{0, -1});
        directionMoveMap.put(Direction.East, new Integer[]{1, 0});
        directionMoveMap.put(Direction.West, new Integer[]{-1, 0});
    }


    public Move(){
        initDirectionMoveMap();
    }

    @Override
    public void execute(Location location) {
        if (location != null){
            Integer[] dir = directionMoveMap.get(location.getDirection());
            location.setX(location.getX()+ dir[0]);
            location.setY(location.getY()+ dir[1]);
        }

    }
}
