package com.afs.tdd;

import java.util.ArrayList;
import java.util.List;

public class Invoker {
    private List<Command> commands;

    public Invoker() {
        commands = new ArrayList<>();
    }

    public List<Command> getCommands() {
        return commands;
    }

    public void setCommands(List<Command> commands) {
        this.commands = commands;
    }

    public void setCommands(Command command){
        this.commands.add(command);
    }

    public void execute(MarsRover marsRover){
        marsRover.executeCommands(commands);
    }

}
