package com.afs.tdd;


import java.util.List;

public class MarsRover {
    private Location location;

    public MarsRover(Location location) {
        this.location = location;
    }


    public void executeCommands(List<Command> commands){
        if(commands != null && commands.size() != 0){
            commands.forEach(command -> command.execute(location));
        }
    }

    public Location getLocation() {
        return location;
    }

}
